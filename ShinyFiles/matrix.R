library(shiny)                          # load shiny at beginning at both scripts
library(MASS)

ui <- fluidPage(                   # standard shiny layout, controls on the
  # left, output on the right
  titlePanel("Matrix"),                 # give the interface a title
  sidebarPanel(                                   # all the UI controls go in here
    
    h4("First Matrix"),
    
    numericInput("ro", "Enter number of rows of matrix", 3, min = 0, step = 1),
    
    numericInput("co", "Enter number of columns of matrix", 3, min = 0, step = 1),
    
    textInput('mat1', 'Enter the matrix row- wise (comma delimited)', "1,2,3,4,5,6,7,8,9"),
    
    textInput("oper", "Enter operation to be evaluated on Matrix 1:", "mean"),
    
    radioButtons("rc", "Along?", choices = c("Row" = "row", "Column" = "col")),
    
    h4("Second Matrix"),
    
    numericInput("CO", "Enter number of columns of matrix", 3, min = 0, step = 1),
    
    textInput('mat2', 'Enter the matrix row- wise (comma delimited)(Optional)', ""),
    
    actionButton("goButton", "Go!")
    
  ),
  
  mainPanel(                                                   # all of the output elements go in here
    
    h3("Output:"),                                   # title with HTML helper
    
    h4("Matrix 1:"),
    verbatimTextOutput("oid1"),
    
    h4("Operation on Matrix 1:"),
    verbatimTextOutput("oid8"),
    
    h4("Matrix 2:"),
    verbatimTextOutput("oid2"),
    
    h4("Product of Matrices"),
    verbatimTextOutput("oid3"),
    
    h4("G-inverse of Product:"),
    verbatimTextOutput("oid4"),
    
    h4("Eigen-decomposition of Product:"),
    verbatimTextOutput("oid5"),
    
    h4("QR Decomposition of Product:"),
    verbatimTextOutput("oid6"),
    
    h4("SVD of Product:"),
    verbatimTextOutput("oid7")
    
  )
)

server <- function(input, output) {
  
  #X <- reactive(matrix(as.numeric(unlist(strsplit(input$mat1,","))), nrow = input$ro, 
  #            ncol = input$co, byrow = TRUE))
  
  output$oid1 <- renderPrint({
    
    input$goButton
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    print(X)
    
  })
  
  output$oid2 <- renderPrint({
    
    input$goButton
    if(input$mat2 == ""){
      Y <- matrix(sample(-50:50,isolate(input$co * input$CO)), nrow = isolate(input$co), 
                  ncol = isolate(input$CO))
    }else{
      Y <- matrix(as.numeric(unlist(strsplit(isolate(input$mat2),","))), nrow = isolate(input$co), 
                  ncol = isolate(input$CO), byrow = TRUE)
    }
    print(Y)
    
  })
  
  output$oid3 <- renderPrint({
    
    input$goButton
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    if(input$mat2 == ""){
      Y <- matrix(sample(-50:50,isolate(input$co * input$CO)), nrow = isolate(input$co), 
                  ncol = isolate(input$CO))
    }else{
      Y <- matrix(as.numeric(unlist(strsplit(isolate(input$mat2),","))), nrow = isolate(input$co), 
                  ncol = isolate(input$CO), byrow = TRUE)
    }
    print(X%*%Y)
    
  })
  
  output$oid4 <- renderPrint({
    
    input$goButton
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    if(input$mat2 == ""){
      Y <- matrix(sample(-50:50,isolate(input$co * input$CO)), nrow = isolate(input$co), 
                  ncol = isolate(input$CO))
    }else{
      Y <- matrix(as.numeric(unlist(strsplit(isolate(input$mat2),","))), nrow = isolate(input$co), 
                  ncol = isolate(input$CO), byrow = TRUE)
    }
    print(ginv(X%*%Y))
  })
  
  output$oid5 <- renderPrint({
    
    input$goButton
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    if(input$mat2 == ""){
      Y <- matrix(sample(-50:50,isolate(input$co * input$CO)), nrow = isolate(input$co), 
                  ncol = isolate(input$CO))
    }else{
      Y <- matrix(as.numeric(unlist(strsplit(isolate(input$mat2),","))), nrow = isolate(input$co), 
                  ncol = isolate(input$CO), byrow = TRUE)
    }
    print(eigen(X%*%Y))
  })
  
  output$oid6 <- renderPrint({
    
    input$goButton
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    if(input$mat2 == ""){
      Y <- matrix(sample(-50:50,isolate(input$co * input$CO)), nrow = isolate(input$co), 
                  ncol = isolate(input$CO))
    }else{
      Y <- matrix(as.numeric(unlist(strsplit(isolate(input$mat2),","))), nrow = isolate(input$co), 
                  ncol = isolate(input$CO), byrow = TRUE)
    }
    print(qr(X%*%Y))
  })
  
  output$oid7 <- renderPrint({
    
    input$goButton
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    if(input$mat2 == ""){
      Y <- matrix(sample(-50:50,isolate(input$co * input$CO)), nrow = isolate(input$co), 
                  ncol = isolate(input$CO))
    }else{
      Y <- matrix(as.numeric(unlist(strsplit(isolate(input$mat2),","))), nrow = isolate(input$co), 
                  ncol = isolate(input$CO), byrow = TRUE)
    }
    print(svd(X%*%Y))
  })
  
  output$oid8 <- renderPrint({
    
    X <- matrix(as.numeric(unlist(strsplit(isolate(input$mat1),","))), nrow = isolate(input$ro), 
                ncol = isolate(input$co), byrow = TRUE)
    input$goButton
    print(isolate(apply(X, 1+(input$rc == "col"),input$oper)))
  })
  
}

shinyApp(ui, server)