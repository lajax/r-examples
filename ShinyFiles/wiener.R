library(shiny)                         

ui <- fluidPage(                   
  
	titlePanel("Wiener Process"),                 
	sidebarPanel(                                   
	
		sliderInput("r", "Sample Paths:",
			min = 10, max = 1000, value = 500, step = 5),
  
		sliderInput("int", "Interval:",
		            min = 0, max = 100, value = c(0,1)),
		
		sliderInput("t1", "t1:",
		            min = 10, max = 1000, value = 100, step = 5),
		
		sliderInput("t2", "t2:",
		            min = 10, max = 1000, value = 200, step = 5),
		
		sliderInput("t3", "t3:",
		            min = 10, max = 1000, value = 400, step = 5),
		
		sliderInput("t4", "t4:",
		            min = 10, max = 1000, value = 800, step = 5)
				
  ),

  mainPanel(                                               # all of the output elements go in here

	  h3("Output:"),                                   # title with HTML helper
	  plotOutput("plot")
	           
  )

)

server <- function(input, output) {                   # server is defined within these parentheses

	output$plot <- renderPlot({
	
	  x <- numeric(0)
	  t <- c(input$t1, input$t2, input$t3, input$t4)
	  
	  inc <- diff(input$int)/max(t)                       #increments
	  
	  result <- data.frame(replicate(length(t),numeric(input$r)))
	  
	  for(i in 1:input$r){
	    x <- rnorm(max(t), 0, sqrt(inc))
	    for(j in 1:length(t)){
	      result[i,j] <- sum(x[1:t[j]])
	    }
	  }
	  
	  A <- data.frame(col = as.vector(as.matrix(result)), 
	                  fact = rep(paste("t",seq_along(t), "=", t, sep = ""), each = input$r))
	  
	  densityplot(~col , data = A, groups = fact, plot.points = FALSE, 
	              main = "First Order Probability Distribution", ref = TRUE, 
	              auto.key = list(columns = 4))
	  
	})
	  
}

shinyApp(ui, server)