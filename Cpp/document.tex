\documentclass{article}
\usepackage{geometry} % Required for adjusting page dimensions and margins
\geometry{
	paper=a4paper, % Paper size, change to letterpaper for US letter size
	top=3cm, % Top margin
	bottom=3cm, % Bottom margin
	left=3cm, % Left margin
	right=3cm, % Right margin
	headheight=14pt, % Header height
	footskip=1.4cm, % Space from the bottom margin to the baseline of the footer
	headsep=10pt, % Space from the top margin to the baseline of the header
	%showframe, % Uncomment to show how the type block is set on the page
}
\usepackage{fancyvrb}
\fvset{tabsize=4}
\usepackage{booktabs}
\usepackage{longtable}

\begin{document}
	\section{Standard Input and Output Objects}
	
	The library defines four IO objects. To handle input, we use an object of type istream named \textbf{cin}. This object is also referred to as the standard input. For output, we use an ostream object named \textbf{cout}. This object is also known as the standard output. The library also defines two other ostream objects, named \textbf{cerr} and \textbf{clog}.
	
	\begin{Verbatim}
	#include <iostream>
	int main()
	{
		std::cout << "Enter two numbers:" << std::endl;
		int v1 = 0, v2 = 0;
		std::cin >> v1 >> v2;
		std::cout << "The sum of " << v1 << " and " << v2 << " is " << v1 + v2 << 
		std::endl;
		return 0;
	}
	\end{Verbatim}
	
	The prefix \texttt{std::} indicates that the names \texttt{cout} and \texttt{endl} are defined inside the namespace named \texttt{std}.
	
	\section{Kinds of Comments in C++}
	
	There are two kinds of comments in C++: single-line and paired. A single-line comment starts with a double slash (//) and ends with a newline. Everything to the right of the slashes on the current line is ignored by the compiler. A comment of this kind can contain any text, including additional double slashes.
	
	The other kind of comment uses two delimiters (/* and */) that are inherited from C. Such comments begin with a /* and end with the next */. These comments can include anything that is not a */, including newlines. The compiler treats everything that falls between the /* and */ as part of the comment.
	
	
	\begin{Verbatim}
	#include <iostream>
	int main()
	{
		int sum = 0, value = 0;
		// read until end-of-file, calculating a running total of all values read
		while (std::cin >> value)
		sum += value;
		std::cout << "Sum is: " << sum << std::endl;
		return 0;
	}
	\end{Verbatim}
	
	We can use an \texttt{if} to write a program to count how many consecutive times each distinct value appears in the input:
	
	\begin{Verbatim}
	#include <iostream>
	int main()
	{
		// currVal is the number we’re counting; we’ll read new values into val
		int currVal = 0, val = 0;
		// read first number and ensure that we have data to process
		if (std::cin >> currVal) {
			int cnt = 1; // store the count for the current value we’re processing
			while (std::cin >> val) { // read the remaining numbers
				if (val == currVal)
				++cnt;
				else { // otherwise, print the count for the previous value
					std::cout << currVal << " occurs " << cnt << " times" << 
					std::endl;
					currVal = val; // remember the new value
					cnt = 1; // reset the counter
				}
			}
			// remember to print the count for the last value in the file
			std::cout << currVal << " occurs " << cnt << " times" << std::endl;
		}
		return 0;
	}
	\end{Verbatim}
	
	\begin{longtable}[c]{@{}p{5em}p{13em}p{9em}@{}}
		\toprule
		\textbf{Type}  & \textbf{Meaning}  & \textbf{Minimum Size} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		bool  & boolean  & NA \\
		char  & character  & 8 bits \\
		wchar\_t  & wide character  & 16 bits \\
		char16\_t  & Unicode character  & 16 bits \\
		char32\_t  & Unicode character  & 32 bits \\
		short  & short integer  & 16 bits \\
		int   & integer  & 16 bits \\
		long  & long integer  & 32 bits \\
		long long  & long integer  & 64 bits \\
		float  & single-precision floating-point  & 6 significant digits \\
		double  & double-precision floating-point  & 10 significant digits \\
		long double  & extended-precision floating-point  & 10 significant digits \\
		\bottomrule
	\end{longtable}
	
	\section{Signed and Unsigned Types}
	
	Except for bool and the extended character types, the integral types may be signed or unsigned. A signed type represents negative or positive numbers (including zero); an unsigned type represents only values greater than or equal to zero.
	
	The types \texttt{int, short, long, and long long} are all signed. We obtain the corresponding unsigned type by adding unsigned to the type, such as \texttt{unsigned long}. The type \texttt{unsigned int} may be abbreviated as \texttt{unsigned}.
	
	\section{Escape Sequences}
	
	Some characters, such as backspace or control characters, have no visible image. Such characters are nonprintable.
	
	The language defines several escape sequences:
	\begin{Verbatim}
	newline \n 	horizontal tab \t 	alert (bell) \a
	vertical tab \v 	backspace \b 	double quote \"
	backslash \\ 	question mark \? 	single quote \’
	carriage return \r 	formfeed \f
	\end{Verbatim}
	
	\section{Library string Type}
	
	A string is a variable-length sequence of characters. To use the string type, we must include the string header. Because it is part of the library, string is defined in the std namespace.
	
	\begin{Verbatim}
	#include <string>
	using std::string;
	\end{Verbatim}
	
	\begin{longtable}[c]{@{}p{9em}p{21em}@{}}
		\toprule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		string s1  & Default initialization; s1 is the empty string. \\
		string s2(s1)  & s2 is a copy of s1. \\
		string s2 = s1  & Equivalent to s2(s1), s2 is a copy of s1. \\
		string s3("value")  & s3 is a copy of the string literal, not including the null. \\
		string s3 = "value"  & Equivalent to s3("value"), s3 is a copy of the string literal. \\
		string s4(n, 'c')  & Initialize s4 with n copies of the character 'c'. \\
		\bottomrule
	\end{longtable}
	
	\begin{Verbatim}
	int main()
	{
		string s; // empty string
		cin >> s; // read a whitespace-separated string into s
		cout << s << endl; // write s to the output
		return 0;
	}
	\end{Verbatim}
	
	\begin{verbatim}
	os << s 	Writes s onto output stream os. Returns os.
	is >> s 	Reads whitespace-separated string from is into s. Returns is.
	getline(is, s) 	Reads a line of input from is into s. Returns is.
	s.empty() 	Returns true if s is empty; otherwise returns false.
	s.size() 	Returns the number of characters in s.
	s[n] 	Returns a reference to the char at position n in s; positions start at 0.
	s1 + s2 	Returns a string that is the concatenation of s1 and s2.
	s1 = s2 	Replaces characters in s1 with a copy of s2.
	<, <=, >, >= 	Comparisons are case-sensitive and use dictionary ordering
	\end{verbatim}
	
	\begin{longtable}[c]{@{}p{6em}p{23em}@{}}
		\toprule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		os $<<$ s  & Writes s onto output stream os. Returns os. \\
		is $>>$ s  & Reads whitespace-separated string from is into s. Returns is. \\
		getline(is, s)  & Reads a line of input from is into s. Returns is. \\
		s.empty()  & Returns true if s is empty; otherwise returns false. \\
		s.size()  & Returns the number of characters in s. \\
		s[n]  & Returns a reference to the char at position n in s; positions start at 0. \\
		s1 + s2  & Returns a string that is the concatenation of s1 and s2. \\
		s1 = s2  & Replaces characters in s1 with a copy of s2. \\
		$<, <=, >, >=$  & Comparisons are case-sensitive and use dictionary ordering \\
		\bottomrule
	\end{longtable}
	
	\section{Library vector Type}
	
	A vector is a collection of objects, all of which have the same type. Every object in the collection has an associated index, which gives access to that object. A vector is often referred to as a container because it “contains” other objects.
	
	\begin{Verbatim}
	#include <vector>
	using std::vector;
	
	vector<int> ivec; // ivec holds objects of type int
	vector<Sales_item> Sales_vec; // holds Sales_items
	vector<vector<string>> file; // vector whose elements are vectors
	\end{Verbatim}
	
	\begin{longtable}[c]{@{}p{13em}p{20em}@{}}
		\toprule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		vector$<$T$>$ v1  & vector that holds objects of type T. Default initialization; v1 is empty. \\
		vector$<$T$>$ v2(v1)  & v2 has a copy of each element in v1. \\
		vector$<$T$>$ v2 = v1  & Equivalent to v2(v1), v2 is a copy of the elements in v1. \\
		vector$<$T$>$ v3(n, val)  & v3 has n elements with value val. \\
		vector$<$T$>$ v4(n)  & v4 has n copies of a value-initialized object. \\
		vector$<$T$>$ v5\{a,b,c ... \}  & v5 has as many elements as there are initializers; elements are initialized by corresponding initializers. \\
		vector$<$T$>$ v5 = \{a,b,c ... \}  & Equivalent to v5\{a,b,c ... \}. \\
		\bottomrule
	\end{longtable}
	
	\begin{longtable}[c]{@{}p{8em}p{21em}@{}}
		\toprule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		v.empty()  & Returns true if v is empty; otherwise returns false. \\
		v.size()  & Returns the number of elements in v. \\
		v.push\_back(t)  & Adds an element with value t to end of v. \\
		v[n]  & Returns a reference to the element at position n in v. \\
		v1 = v2  & Replaces the elements in v1 with a copy of the elements in v2. \\
		v1 = \{a,b,c ... \}  & Replaces the elements in v1 with a copy of the elements in the comma-separated list. \\
		$<, <=, >, >=$  & Have their normal meanings using dictionary ordering \\
		\bottomrule
	\end{longtable}
	
	\begin{Verbatim}
	// count the number of grades by clusters of ten: 0-9, 10-19, ... 90-99, 100
	vector<unsigned> scores(11, 0); // 11 buckets, all initially 0
	unsigned grade;
	while (cin >> grade) {// read the grades
		if (grade <= 100) // handle only valid grades
		++scores[grade/10]; // increment the counter for the current cluster
	}
	\end{Verbatim}
	
	\section{Arrays}
	
	An array is a data structure that is similar to the library vector type but offers a different trade-off between performance and flexibility. Like a vector, an array is a container of unnamed objects of a single type that we access by position. Unlike a vector, arrays have fixed size; we cannot add elements to an array. Because arrays have fixed size, they sometimes offer better run-time performance for specialized applications. However, that run-time advantage comes at the cost of lost flexibility.
	
	Arrays are a compound type. An array declarator has the form \texttt{a[d]}, where a is the name being defined and d is the dimension of the array. The dimension specifies the number of elements and must be greater than zero. The number of elements in an array is part of the array’s type. As a result, the dimension must be known at compile time, which means that the dimension must be a constant expression
	
	\begin{Verbatim}
	unsigned cnt = 42; // not a constant expression
	constexpr unsigned sz = 42; // constant expression
	int arr[10]; // array of ten ints
	int *parr[sz]; // array of 42 pointers to int
	string bad[cnt]; // error: cnt is not a constant expression
	string strs[get_size()]; // ok if get_size is constexpr, error otherwise
	
	string nums[] = {"one", "two", "three"}; // array of strings
	string *p = &nums[0]; // p points to the first element in nums
	\end{Verbatim}
	
	\section{Introduction to Iterators in C++}
	
	An iterator is an object (like a pointer) that points to an element inside the container. We can use iterators to move through the contents of the container. They can be visualised as something similar to a pointer pointing to some location and we can access content at that particular location using them.
	
	Iterators play a critical role in connecting algorithm with containers along with the manipulation of data stored inside the containers. The most obvious form of iterator is a pointer. A pointer can point to elements in an array, and can iterate through them using the increment operator (++). But, all iterators do not have similar functionality as that of pointers.
	
	Depending upon the functionality of iterators they can be classified into five categories, as shown in the diagram below with the outer one being the most powerful one and consequently the inner one is the least powerful in terms of functionality.
	
	\subsection{Types of iterators:}
	
	Based upon the functionality of the iterators, they can be classified into five major categories:
	
	\begin{enumerate}
		\item \textbf{Input Iterators}: They are the weakest of all the iterators and have very limited functionality. They can only be used in a single-pass algorithms, i.e., those algorithms which process the container sequentially such that no element is accessed more than once.
		
		\item \textbf{Output Iterators}: Just like input iterators, they are also very limited in their functionality and can only be used in single-pass algorithm, but not for accessing elements, but for being assigned elements.
		
		\item \textbf{Forward Iterator}: They are higher in hierarachy than input and output iterators, and contain all the features present in these two iterators. But, as the name suggests, they also can only move in forward direction and that too one step at a time.
		
		\item \textbf{Bidirectional Iterators}: They have all the features of forward iterators along with the fact that they overcome the drawback of forward iterators, as they can move in both the directions, that is why their name is bidirectional.
		
		\item \textbf{Random-Access Iterators}: They are the most powerful iterators. They are not limited to moving sequentially, as their name suggests, they can randomly access any element inside the container. They are the ones whose functionality is same as pointers
	\end{enumerate}
	
	\section{Iterators in C++ STL}
	
	Iterators are used to point at the memory addresses of STL containers. They are primarily used in sequence of numbers, characters etc. They reduce the complexity and execution time of program.
	
	\subsection{Operations of iterators:-}
	
	\begin{enumerate}
		\item \textbf{begin()} :- This function is used to return the beginning position of the container.
	
		\item \textbf{end()} :- This function is used to return the after end position of the container.
	
		\begin{Verbatim}
		// C++ code to demonstrate the working of
		// iterator, begin() and end()
		#include<iostream>
		#include<iterator> // for iterators
		#include<vector> // for vectors
		using namespace std;
		
		int main()
		{
			vector<int> ar = { 1, 2, 3, 4, 5 };
			// Declaring iterator to a vector
			vector<int>::iterator ptr;
			// Displaying vector elements using begin() and end()
			cout << "The vector elements are : ";
			for (ptr = ar.begin(); ptr < ar.end(); ptr++)
			cout << *ptr << " ";
			return 0;
		}
		\end{Verbatim}
	
		\textbf{Output:}
		\begin{Verbatim}
		The vector elements are : 1 2 3 4 5
		\end{Verbatim}
		
		\item \textbf{advance()} :- This function is used to increment the iterator position till the specified number mentioned in its arguments.
		
		\begin{Verbatim}
		// C++ code to demonstrate the working of advance()
		#include<iostream>
		#include<iterator> // for iterators
		#include<vector> // for vectors
		using namespace std;
		
		int main()
		{
			vector<int> ar = { 1, 2, 3, 4, 5 };
			// Declaring iterator to a vector
			vector<int>::iterator ptr = ar.begin();
			// Using advance() to increment iterator position points to 4
			advance(ptr, 3);
			// Displaying iterator position
			cout << "The position of iterator after advancing is: " << *ptr << 
			" " << endl;
			return 0;
		}
		\end{Verbatim}
		
		\textbf{Output:}
		\begin{Verbatim}
		The position of iterator after advancing is : 4
		\end{Verbatim}
		
		\item \textbf{next()} :- This function returns the new iterator that the iterator would point after advancing the positions mentioned in its arguments.
		
		\item \textbf{prev()} :- This function returns the new iterator that the iterator would point after decrementing the positions mentioned in its arguments.
		
		\begin{Verbatim}
		// C++ code to demonstrate the working of next() and prev()
		#include<iostream>
		#include<iterator> // for iterators
		#include<vector> // for vectors
		using namespace std;
		
		int main()
		{
			vector<int> ar = { 1, 2, 3, 4, 5 };
			// Declaring iterators to a vector
			vector<int>::iterator ptr = ar.begin();
			vector<int>::iterator ftr = ar.end();
			// Using next() to return new iterator points to 4
			auto it = next(ptr, 3);
			// Using prev() to return new iterator points to 3
			auto it1 = prev(ftr, 3);
			// Displaying iterator position
			cout << "The position of new iterator using next() is : " << *it << 
			" " << endl;
			// Displaying iterator position
			cout << "The position of new iterator using prev() is : " << *it1 << 
			" " << endl;
			return 0;
		}
		\end{Verbatim}
		
		\textbf{Output:}
		\begin{Verbatim}
		The position of new iterator using next() is : 4
		The position of new iterator using prev() is : 3
		\end{Verbatim}
		
		\item \textbf{inserter()} :- This function is used to insert the elements at any position in the container. It accepts 2 arguments, the container and iterator to position where the elements have to be inserted.
		
		\begin{Verbatim}
		// C++ code to demonstrate the working of inserter()
		#include<iostream>
		#include<iterator> // for iterators
		#include<vector> // for vectors
		using namespace std;
		
		int main()
		{
			vector<int> ar = { 1, 2, 3, 4, 5 };
			vector<int> ar1 = {10, 20, 30};
			// Declaring iterator to a vector
			vector<int>::iterator ptr = ar.begin();
			// Using advance to set position
			advance(ptr, 3);
			// copying 1 vector elements in other using inserter()
			// inserts ar1 after 3rd position in ar
			copy(ar1.begin(), ar1.end(), inserter(ar,ptr));
			// Displaying new vector elements
			cout << "The new vector after inserting elements is : ";
			for (int &x : ar)
			cout << x << " ";
			return 0;
		}
		\end{Verbatim}
		
		\textbf{Output:}
		\begin{Verbatim}
		The new vector after inserting elements is : 1 2 3 10 20 30 4 5
		\end{Verbatim}
	\end{enumerate}
	
	\section{Standard Template Library}
	
	\subsection{$<$cmath$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		pow()  & Computes Power a Number \\
		llrint()  & Rounds argument using current rounding mode \\
		remainder()  & Returns remainder of x/y \\
		nan()  & returns a quiet NaN value \\
		cosh()  & Returns Hyperbolic Cosine of an Angle \\
		copysign()  & returns num with value of first and sign of second \\
		fma()  & Returns Fused Multiply– Accumulate \\
		abs()  & returns absolute value of an argument \\
		fabs()  & returns absolute value of argument \\
		fdim()  & Returns Positive Difference Between Arguments \\
		fmin()  & returns smallest among two given arguments \\
		fmax()  & returns largest among two arguments passed \\
		hypot()  & Returns Square Root of sum of square of Arguments \\
		nexttoward()  & returns next value after x in direction of y \\
		nextafter()  & returns next value after x in direction of y \\
		cbrt()  & Computes Cube Root of a Number \\
		sqrt()  & Computes Square Root of A Number \\
		remquo()  & Computer remainder and stores quotient of x/y \\
		logb()  & returns logarithm of $|$x$|$ \\
		log1p()  & returns natural logarithm of x+1. \\
		scalbln()  & Scales x by FLT\_RADIX to the power n \\
		log2()  & returns base 2 logarithm of a number \\
		scalbn()  & Scales x by FLT\_RADIX to the power n \\
		ilogb()  & returns integral part of logarithm of |x| \\
		nearbyint()  & Rounds argument to using current rounding mode \\
		expm1()  & Returns e raised to Power Minus 1 \\
		ldexp()  & returns product of x and 2 raised to the power e \\
		frexp()  & breaks float to its binary significand \\
		exp2()  & Returns 2 raised to a Number \\
		exp()  & returns exponential (e) raised to a number \\
		modf()  & Breaks Number Into Integral and Fractional Part \\
		log10()  & Returns Base 10 Logarithm of a Number \\
		lrint()  & Rounds argument using current rounding mode \\
		rint()  & Rounds argument using current rounding mode \\
		llround()  & Rounds argument to nearest long long int value \\
		lround()  & Returns the long int value nearest to the argument \\
		round()  & Returns integral value nearest to argument \\
		trunc()  & Truncates the demical part of a number \\
		log()  & Returns Natural Logarithm of a Number \\
		atanh()  & returns arc hyperbolic tangent of a number \\
		asinh()  & returns arc hyperbolic sine of a number \\
		acosh()  & returns hyperbolic cosine of a number \\
		fmod()  & Computes floating point remainder of division \\
		tanh()  & returns hyperbolic tangent of an angle \\
		floor()  & Returns floor value of decimal number \\
		ceil()  & Return ceiling value of number \\
		sinh()  & returns hyperbolic sine of an angle \\
		acos()  & Returns Inverse cosine a Number \\
		atan2()  & Returns Inverse Tangent of a Coordinate \\
		tan()  & Returns Tangent of the Argument \\
		atan()  & Returns Inverse tangent a Number \\
		asin()  & Returns Inverse Sine a Number \\
		sin()  & Returns Sine of the Argument \\
		cos()  & Returns Cosine of the Argument \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cstdlib$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		calloc()  & allocates block of memory and initializes to zero \\
		wcstombs()  & converts wide character string to multibyte seq \\
		mbstowcs()  & converts multibyte char string to wide char seq \\
		wctomb()  & converts wide character to a multibyte character \\
		mbtowc()  & converts multibyte character to a wide character \\
		mblen()  & determines size of a multibyte character \\
		lldiv()  & computes integral division of two long long int \\
		llabs()  & returns absolute value of a long long int data \\
		ldiv()  & computes integral division of long int numbers \\
		labs()  & returns absolute value of long or long int number \\
		abs()  & returns absolute value of an integer \\
		div()  & computes integral quotient and remainder of number \\
		qsort()  & sorts array using quicksort algorithm \\
		bsearch()  & performs binary search on sorted array \\
		\_Exit()  & causes termination without cleanup tasks \\
		quick\_exit()  & causes termination without cleaning resources \\
		getenv()  & returns pointer to environment variable passed \\
		at\_quick\_exit()  & registers function and calls on quick termination \\
		atexit()  & registers function to be called on termination \\
		realloc()  & reallocates a block of previously allocated memory \\
		malloc()  & allocates a block of unitialized memory \\
		free()  & deallocates a block of memory \\
		srand()  & seeds pseudo random number for rand() \\
		strtoull()  & converts string to unsigned long long int \\
		strtoll()  & converts string to long long int in C++ \\
		atol()  & Converts String to Integer \\
		strtol()  & Converts a string to number \\
		atof()  & Converts String to Double \\
		strtod()  & returns string float to double \\
		\bottomrule
	\end{longtable}

	\subsection{$<$iostream$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		wclog  & writes to log stream with wide character \\
		wcerr  & prints to error stream as wide character type \\
		wcout  & displays wide characters (Unicode) to screen \\
		wcin  & accepts input in wide character type \\
		clog  & used for streaming logs \\
		cerr  & writes to error stream \\
		cout  & displays output to output device i.e monitor \\
		cin   & accepts input from user \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cstring$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		strxfrm()  & transform byte string into implementation def form \\
		strcoll()  & compares two null terminated string \\
		strlen()  & returns length of given string \\
		strerror()  & gives description of system error code \\
		memset()  & copies character to beginning of string n times \\
		strtok()  & split string based on delimiter \\
		strstr()  & finds first occurrence of a substring in string \\
		strspn()  & gives length of maximum initial segment \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cctype$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		toupper()  & converts a given character to uppercase \\
		tolower()  & converts a given character to lowercase \\
		isxdigit()  & checks if given character is hexadecimal character \\
		isupper()  & check if given character is uppercase or not \\
		isspace()  & check if given character is whitespace character \\
		ispunct()  & check if given character is punctuation character \\
		isprint()  & check if given character is printable or not \\
		islower()  & checks if given character is lowercase \\
		isgraph()  & checks if given character is graphic or not \\
		isdigit()  & checks if given character is a digit or not \\
		iscntrl()  & checks if given character is control character \\
		isblank()  & checks if given character is a blank character \\
		isalpha()  & checks if given character is alphabet or not \\
		\bottomrule
	\end{longtable}

	\subsection{$<$csignal$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		raise()  & sends signal to the program \\
		signal()  & sets error handler for specified signal \\
		\bottomrule
	\end{longtable}
	
	\subsection{$<$clocale$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		localeconv()  & returns current locale formatting rules \\
		setlocale()  & sets locale information for the current program \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cwctype$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		iswdigit()  & checks if given wide character is digit or not \\
		wctype()  & returns wide character classifcation \\
		wctrans()  & returns current transformation for wide character \\
		towctrans()  & transforms a given wide character \\
		iswctype()  & checks if given wide char has certain property \\
		towupper()  & converts given wide character to uppercase \\
		towlower()  & converts given wide character to lowercase \\
		iswxdigit()  & checks if given wide character is hexadecimal num \\
		iswupper()  & checks if given wide character is uppercase \\
		iswspace()  & checks if given wide character is wide whitespace \\
		iswpunct()  & checks if given wide character is punctuation \\
		iswprint()  & checks if given wide character can be printed \\
		iswlower()  & checks if given wide character is lowercase \\
		iswgraph()  & checks if wide char has graphical representation \\
		iswcntrl()  & checks if given wide char is control character \\
		iswblank()  & checks if given wide character is blank character \\
		iswalpha()  & checks if given wide character is an alphabet \\
		iswalnum()  & checks if given wide character is alphanumeric \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cstdio$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		getc()  & reads next character from input stream \\
		fseek()  & sets file position indicator for given file stream \\
		ungetc()  & push previously read character back to the stream \\
		vsscanf()  & read data from a string buffer \\
		vscanf()  & read data from stdin \\
		vfscanf()  & read data from a file stream \\
		freopen()  & opens a new file with stream associated to another \\
		fflush()  & flushes any buffered data to the respective device \\
		setvbuf()  & change or specify buffering mode and buffer size \\
		perror()  & prints error to stderr \\
		ferror()  & checks for errors in given stream \\
		feof()  & function checks if file stream EOF has been reached or not \\
		clearerr()  & resets error flags and EOF indicator for stream \\
		rewind()  & sets file position to beginning of stream \\
		ftell()  & returns current position of file pointer \\
		fsetpos()  & sets stream file pointer to given position \\
		fgetpos()  & gets current file position \\
		fwrite()  & writes specified number of characters to stream \\
		fread()  & reads specified no. of characters from stream \\
		puts()  & writes string to stdout \\
		putchar()  & writes a character to stdout \\
		putc()  & writes character to given output stream \\
		gets()  & reads line from stdin \\
		getchar()  & reads next character from stdin \\
		fputs()  & writes string to file stream \\
		fputc()  & writes character to given output stream \\
		fgets()  & reads n number of characters from file stream \\
		fgetc()  & reads the next character from given input stream \\
		vsprintf()  & write formatted string to a string buffer \\
		vsnprintf()  & write formatted string to string buffer \\
		vprintf()  & printf but takes args from vlist instead \\
		vfprintf()  & write formatted string to file stream \\
		sscanf()  & read data from string buffer \\
		sprintf()  & write a formatted string to buffer \\
		snprintf()  & write formatted string to character string buffer \\
		scanf()  & read data form stdin \\
		printf()  & write formatted string to stdout \\
		fscanf()  & read data from file stream \\
		fprintf()  & write a formatted string to file stream \\
		setbuf()  & sets the internal bufer to be used for I/O \\
		fopen()  & opens specified file \\
		fclose()  & closes given file stream \\
		tmpnam()  & generates unique filename \\
		tmpfile()  & creates temporary file with auto-generated name \\
		rename()  & renames or moves specified file \\
		remove()  & deletes the specified file \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cwchar$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		wcscoll()  & compares two null terminated wide string \\
		wcstoull()  & converts wide string num to unsigned long long \\
		wcstoul()  & converts wide str of given base to unsigned long \\
		wcstoll()  & converts wide string of specified base to int \\
		wcsftime()  & converts given date and time to wide character str \\
		wmemset()  & copies single wide char for a certain num of time \\
		wmemmove()  & moves wide chars from src to dest \\
		wmemcpy()  & copies specified num of wide char from src to dest \\
		wmemcmp()  & compares wide chars of two wide strings \\
		wmemchr()  & searches for first occurrence of wide char \\
		wcsxfrm()  & transforms wide string to implementation defined \\
		wcsstr()  & finds first occurrence of wide substring in a str \\
		wcsspn()  & returns length of maximum initial segment \\
		wcsrchr()  & searches last occurrence of wide char in string \\
		wcspbrk()  & searches for set of wide char in given wide string \\
		wcsncpy()  & copies specified number of wide characters \\
		wcsncmp()  & compares specified number of wide char of strings \\
		wcsncat()  & appends specified num of wide char to another str \\
		wcslen()  & returns length of the given wide string \\
		wcscspn()  & returns number of wide char before first occurence \\
		wcscpy()  & copies wide character string from source to dest \\
		wcscmp()  & lexicographically compares two wide string \\
		wcschr()  & searches for a wide character in a wide string \\
		wcscat()  & appends copy of wide string to the end of another \\
		wcsrtombs()  & convert wide char seq to narrow multibyte char seq \\
		wctob()  & converts wide character to single byte character \\
		wcrtomb()  & convert wide character to its narrow multibyte rep \\
		mbsrtowcs()  & convert narrow multibyte char seq to wide char seq \\
		mbsinit()  & describe initial conversion state of mbstate\_t obj \\
		mbrtowc()  & converts narrow multibyte char to wide char \\
		mbrlen()  & determines size in bytes of a multibyte character \\
		btowc()  & converts character to its wide character \\
		wcstok()  & returns next token in null terminated wide string \\
		wcstold()  & converts wide string float number to long double \\
		wcstol()  & converts wide string float number to long int \\
		wcstof()  & converts wide string float number to float \\
		wcstod()  & converts wide string float number to double \\
		wscanf()  & reads wide character from stdin \\
		wprintf()  & write formatted wide string to stdout \\
		vwscanf()  & read wide character from stdin \\
		vwprintf()  & write formatted wide string to stdout \\
		vswscanf()  & read wide character string from wide string buffer \\
		vswprintf()  & write formatted wide string to wide string buffer \\
		vfwscanf()  & read wide character string from a file stream \\
		vfwprintf()  & write formatted wide string to a file stream \\
		ungetwc()  & push previously read wide character back to stream \\
		swscanf()  & reads wide character from wide string buffer \\
		swprintf()  & write formatted wide string to wide string buffer \\
		putwchar()  & writes wide character to stdout \\
		putwc()  & writes wide character to the given output stream \\
		getwchar()  & reads next wide character from stdin \\
		getwc()  & reads next wide character from input stream \\
		fwscanf()  & reads wide character from file stream \\
		fwprintf()  & write formatted wide string to a file stream \\
		fwide()  & set or query orientation of given file stream \\
		fputws()  & writes wide string except null wide char to output \\
		fputwc()  & writes wide character to the given output stream \\
		fgetws()  & reads specifed num of wide characters from stream \\
		fgetwc()  & reads next wide character from given input stream \\
		\bottomrule
	\end{longtable}

	\subsection{$<$cuchar$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		mbrtoc32()  & converts narrow multibyte char to 32 bit char \\
		mbrtoc16()  & converts narrow multibyte char to 16 bit char \\
		c32rtomb()  & converts 32 bit char to narrow multibyte char \\
		c16rtomb()  & converts 16 bit char to narrow multibyte char \\
		\bottomrule
	\end{longtable}

	\subsection{$<$csetjmp$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		longjmp() and setjmp() & restores previously saved environment \\
		\bottomrule
	\end{longtable}
	
	\subsection{$<$cfenv$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		fetestexcept()  & tests floating point exception \\
		feupdateenv()  & updates floating point environment \\
		feholdexcept()  & saves and clear floating point status flags \\
		fesetenv()  & set floating point environment \\
		fesetround()  & set rounding direction \\
		fegetenv()  & store status of floating point env in an object \\
		fegetround()  & gets round direction mode \\
		fesetexceptflag()  & sets given floating point exceptions to the env \\
		fegetexceptflag()  & gets floating point exception flags \\
		feraiseexcept()  & raises floating point exceptions specified \\
		feclearexcept()  & attempts to clear floating point exception flags \\
		\bottomrule
	\end{longtable}

	\subsection{$<$ctime$>$}
	
	\begin{longtable}[c]{@{}p{7em}p{21em}@{}}
		\toprule
		\textbf{Title}  & \textbf{Description} \\
		\midrule
		\endhead
		%
		\bottomrule
		\endfoot
		%
		\endlastfoot
		%
		strftime()  & converts calendar time to multibyte character str \\
		mktime()  & converts local calendar time to time since epoch \\
		localtime()  & converts given time since epoch to local time \\
		gmtime()  & converts given time since epoch to UTC time \\
		ctime()  & converts time since epoch to char representation \\
		asctime()  & converts calendar time to character representation \\
		time()  & returns current calendar time \\
		difftime()  & computes difference between two times in seconds \\
		clock()  & returns processor time consumed by program \\
		\bottomrule
	\end{longtable}
\end{document}